const saveFile = async (files, nameFile, typeFile) => {
    const file = files[nameFile];
    const response = { isOk: false, error: null, newName: null }
    if (file.mimetype == typeFile) {
        //Si es pdf guardamos el archivo
        const now = Date.parse(Date());
        response.newName = now + ".pdf";

        response.error = await file.mv("./expedientes/" + response.newName)
        if (response.error) {
            return response;
        } else {
            response.isOk = true;
            return response;
        }

    } else {
        response.error = "El archivo debe de ser formato PDF"
        return response;
    }

}
module.exports = saveFile;