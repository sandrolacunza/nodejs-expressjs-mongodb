const Profesor = require("../Models/Profesor");
const jwt = require("jsonwebtoken");

const authProfesor = async (req, res, next) => {

    const strToken = req.headers.authorization
    if (!strToken) {
        res.status(400).json({ msg: "No se encontro un token" })
    }
    try {
        const token = strToken.split(" ")[1];
        const palabra = process.env.APP_SECRET;
        const llave = jwt.verify(token, palabra);
        const profesor = await Profesor.findById(llave._id);
        if (!profesor) {
            return res.status(400).json({ msg: "Entrego un token no válido" })
        }
        next();
    } catch (error) {
        return res.json(error)
    }

}
module.exports = authProfesor;