const mongoose = require('mongoose');

const connectionDB = async () => {
    try {
        const DB = await mongoose.connect('mongodb://localhost:27017/test_estudiantes', { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
        console.log("Conección realizada con exito ", DB.connection.name);
    } catch (error) {
        console.log(error);
    }
}
module.exports = connectionDB;