
const app = require("./app")
app.listen(app.get("port"), () => {
    console.log("Nombre:", app.get("name"))
    console.log(`Example app listening at http://localhost:${app.get("port")}`)
})