const express = require('express');
const morgan = require("morgan");
const connectionDB = require("./db.connection")
require('dotenv').config()
const routerEstudiantes = require("./routes/estudiantes.routes");
const routerMaterias = require("./routes/materias.routes");
const routerProfesor = require("./routes/profesores.routes");

const fileUpload = require("express-fileupload");
const app = express();

//conexión a DB
connectionDB();
//Setting
app.set("name", "APIRest NodeJs");
app.set("port", process.env.port || 3500);

//middleware
app.use(express.json())
app.use(morgan("dev"))
app.use(fileUpload({
    createParentPath: true,
}));
//Call routes
app.use(express.static("public"))
app.use("/api/estudiantes/", routerEstudiantes);
app.use("/api/materias/", routerMaterias);
app.use("/api/profesores/", routerProfesor);


module.exports = app;