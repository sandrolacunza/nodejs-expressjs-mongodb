const { Router } = require("express");
const ctrStudent = require("../controllers/estudiantes.controller")
const authProfesor = require("../auth/auth.profesor")
const routerEstudiantes = Router();

routerEstudiantes.get('/', authProfesor, ctrStudent.fetchStudent)

routerEstudiantes.post('/', ctrStudent.createStudent)

routerEstudiantes.put('/:id', authProfesor, ctrStudent.updateStudent)

routerEstudiantes.delete('/:id', authProfesor, ctrStudent.deleteStudent)

module.exports = routerEstudiantes;