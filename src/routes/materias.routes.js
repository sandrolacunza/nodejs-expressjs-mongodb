const { Router } = require("express");
const ctrMaterias = require("../controllers/materias.controller");

const routerMaterias = Router();

routerMaterias.get("/:idEstudiante", ctrMaterias.fetchMaterias);
routerMaterias.post("/:idEstudiante", ctrMaterias.addMaterias);
routerMaterias.delete("/:idEstudiante/:idMateria", ctrMaterias.deleteMaterias);
routerMaterias.put("/:idEstudiante/:idMateria", ctrMaterias.updateMaterias);
module.exports = routerMaterias;