const { Router } = require("express")
const ctrProfesor = require("../controllers/profesor.controller")

const routerProfesor = Router();

routerProfesor.post("/register", ctrProfesor.register)
routerProfesor.post("/login", ctrProfesor.login)
module.exports = routerProfesor;