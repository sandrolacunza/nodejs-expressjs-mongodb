const { model, Schema } = require("mongoose");

const ProfesorEsquema = new Schema({
    correo: {
        required: true,
        unique: true,
        type: String
    },
    clave: {
        required: true,
        type: String
    }
});

module.exports = model("Profesor", ProfesorEsquema);