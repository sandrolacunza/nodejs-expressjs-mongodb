const { model, Schema } = require("mongoose");

const EstudianteSchema = new Schema({
    nombre: {
        type: String,
        required: true
    },
    apellido: {
        type: String,
        required: true
    },
    correo: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        default: true
    },
    nameFile: {
        type: String,
        required: true
    },
    materias: [
        {
            nota: Number,
            nombre: String,
            comentario: String
        }
    ],


});
module.exports = model("Estudiante", EstudianteSchema)