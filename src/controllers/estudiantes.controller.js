const Estudiante = require("../Models/Estudiante");
const saveFile = require("../utils/saveFile")

exports.fetchStudent = async (req, res) => {
    try {
        const estudiantes = await Estudiante.find({ active: true });
        res.status(200).json(estudiantes);
    } catch (error) {
        res.status(500).json(error);
    }

};

exports.createStudent = async (req, res) => {
    if (req.files) {
        const resp = await saveFile(req.files, "expediente", "application/pdf"); //se sube el archivo
        const nameFile = resp.newName;
        if (resp.isOk) {
            try {
                const { nombre, apellido, correo, materias } = req.body;
                if (nombre && apellido && correo) {
                    const newEstudiante = new Estudiante({ nombre, apellido, correo, materias, nameFile });
                    await newEstudiante.save();
                    res.status(200).json({
                        id: newEstudiante._id,
                        msg: "Datos recibidos"
                    });
                } else {
                    res.status(400).json({ isOk: false, message: "Los datos son requeridos" })
                }

            } catch (error) {
                res.status(500).json(error)
            }
        } else {
            return res.status(400).json({ isOk: resp.isOk, message: resp.error })
        }
    } else {
        res.status(400).json({ isOk: false, message: "No subio el archivo del estudiante" })
    }

};

exports.updateStudent = async (req, res) => {
    const id = req.params.id;
    const data = req.body;
    try {
        if (id && data) {
            await Estudiante.findByIdAndUpdate(id, data)
            res.status(200).json({ msg: "Datos modificados correctamente", isOK: true })
        } else {
            res.status(500).json({ msg: "Los datos no pasaron la validación", isOk: false })
        }
    } catch (error) {
        res.status(500).json({ msg: "Se produjo un error" })
    }

};

exports.deleteStudent = async (req, res) => {
    try {
        const id = req.params.id;
        const eliminado = await Estudiante.findByIdAndUpdate(id, { active: false });
        res.status(200).json({ msg: 'Se eliminó los datos del ID: ' + eliminado._id, isOk: true })
    } catch (error) {
        res.status(500).json(error)
    }

}