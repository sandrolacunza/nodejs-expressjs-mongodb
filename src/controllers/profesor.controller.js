const Profesor = require("../Models/Profesor")
const jwt = require("jsonwebtoken")

exports.register = async (req, res) => {
    try {
        const { correo, clave } = req.body
        if (correo, clave) {
            const newProfesor = new Profesor({ correo, clave })
            await newProfesor.save()
            res.status(200).json({ msg: "Se registro un nuevo profesor" })
        } else {
            res.status(400).json({ msg: "Todos los datos son requeridos" })

        }
    } catch (error) {
        res.status(500).json({ msg: "Se produjo un error en el servidor" })
    }
}

exports.login = async (req, res) => {
    try {
        const { correo, clave } = req.body;
        if (correo && clave) {
            const profesor = await Profesor.findOne({ correo })
            if (!profesor) {
                res.status(400).json({ token: null, msg: "Usuarios o contraseñas incorrectas" })
            } else {
                if (profesor.clave == clave) {
                    const { _id, correo } = profesor;
                    const opt = {
                        expiresIn: '1h'
                    }
                    const palabra = process.env.APP_SECRET;
                    const token = jwt.sign({ _id, correo }, palabra, opt)
                    res.status(200).json({ token: token })
                } else {
                    res.status(400).json({ token: null, msg: "Usuarios o contraseñas incorrectas" })
                }
            }
        } else {
            res.status(400).json({ msg: "Todos los datos son requeridos" })
        }
    } catch (error) {
        res.status(500).json({ msg: "Se produjo un error en el servidor" })
    }
}