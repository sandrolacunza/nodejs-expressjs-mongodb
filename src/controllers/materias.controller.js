const Estudiante = require('../Models/Estudiante');

exports.fetchMaterias = async (req, res) => {
    try {
        if (req.params.idEstudiante) {
            const idEstudiante = req.params.idEstudiante;
            const estudiante = await Estudiante.findById(idEstudiante);
            res.status(200).json(estudiante.materias);
        } else {
            res.status(400).json({ error: "Debe enviar el id del estudiante" });
        }
    } catch (error) {
        res.status(500).json(error);
    }
}

exports.addMaterias = async (req, res) => {
    try {
        if (req.params.idEstudiante && req.body) {
            const idEst = req.params.idEstudiante;
            const materia = req.body;
            const estudiante = await Estudiante.findById(idEst);
            estudiante.materias.push(materia);
            await estudiante.save();
            res.status(200).json({ msj: "Se agregó la materia correctamente", isOk: true })
        } else {
            res.status(400).json({ errror: "Datos insuficientes" })
        }
    } catch (error) {
        res.status(500).json({ error: error })
    }
}

exports.deleteMaterias = async (req, res) => {
    try {
        if (req.params.idEstudiante && req.params.idMateria) {
            const idMateria = req.params.idMateria;
            const estudiante = await Estudiante.findById(req.params.idEstudiante);
            //recorremos el arreglo de materias del alumno
            for (let index = 0; index < estudiante.materias.length; index++) {
                //console.log(estudiante.materias[index])
                if (estudiante.materias[index]._id == idMateria) {
                    estudiante.materias.splice(index, 1);
                }
            }
            await estudiante.save();
            res.status(200).json({ msj: "Se le eliminó la materia correctamente", isOk: true });
        } else {
            res.status(400).json({ errror: "Datos insuficientes" })
        }
    } catch (error) {
        res.status(500).json({ error: error })
    }
}

exports.updateMaterias = async (req, res) => {
    try {
        if (req.params.idEstudiante && req.params.idMateria && req.body) {
            const idEstudiante = req.params.idEstudiante;
            const idMateria = req.params.idMateria;
            const data = req.body;
            const estudiante = await Estudiante.findById(idEstudiante);

            for (let index = 0; index < estudiante.materias.length; index++) {
                if (estudiante.materias[index]._id == idMateria) {
                    Object.assign(estudiante.materias[index], data)
                }
            }
            await estudiante.save();
            res.status(200).json({ msg: "Se realizó la actualización correctamente", isOk: true })
        } else {
            res.status(400).json({ msg: "Todos los datos son requeridos" });
        }
    } catch (error) {
        res.status(500).json({ msg: "Se produjo un error en el servidor" });
    }
}